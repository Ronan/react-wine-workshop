export const addLike = () => {
  return {
    type: 'ADD_LIKE',
    increment: 1
  };
}

export const removeLike = () => {
  return {
    type: 'REMOVE_LIKE',
    decrement: 1
  };
}

export const setLikes = (likes) => {
  return {
    type: 'SET_LIKES',
    likes
  };
}

export const addComment = () => {
  return {
    type: 'ADD_COMMENT',
    increment: 1
  };
}

export const setComments = (comments) => {
  return {
    type: 'SET_COMMENTS',
    comments
  };
}

export const setTitle = (title) => {
  return {
    type: 'SET_TITLE',
    title
  };
}

export const setRegions = (regions) => {
  return {
    type: 'SET_REGIONS',
    regions
  };
}

export const setWines = (wines) => {
  return {
    type: 'SET_WINES',
    wines
  };
}

export const setCurrentWine = (wine) => {
  return {
    type: 'SET_CURRENT_WINE',
    wine
  };
}

export const setCurrentLiked = (liked) => {
  return {
    type: 'SET_CURRENT_LIKED',
    liked
  };
}

export const setCurrentComments = (comments) => {
  return {
    type: 'SET_CURRENT_COMMENTS',
    comments
  };
}

export const fetchLikesCount = () => {
  return dispatch => {
    fetch(`/api/likes`)
      .then(r => r.json())
      .then(r => dispatch(setLikes(r.count)))
      .catch(response => logError(response));
  };
}

export const fetchCommentsCount = () => {
  return dispatch => {
    fetch(`/api/comments`)
      .then(r => r.json())
      .then(r => dispatch(setComments(r.count)))
      .catch(response => logError(response));
  };
}

export const fetchRegions = () => {
  return dispatch => {
    fetch('/api/regions')
      .then(r => r.json())
      .then(data => dispatch(setRegions(data)))
      .catch(response => logError(response));
  };
}

export const fetchWines = (regionId) => {
  return dispatch => {
    fetch('/api/wines?region='+regionId)
      .then(r => r.json())
      .then(data => dispatch(setWines(data)))
      .catch(response => logError(response));
  }
}

export const fetchCurrentWine = (wineId) => {
  return dispatch => {
    fetch('/api/wines/'+wineId)
      .then(r => r.json())
      .then(data => {
        dispatch(setTitle(data.name));
        dispatch(setCurrentWine(data));
        dispatch(fetchCurrentLiked(wineId));
        dispatch(fetchCurrentComments(wineId));
      })
      .catch(response => logError(response));
    }
}

export const fetchCurrentLiked = (wineId) => {
  return dispatch => {
    fetch('/api/wines/'+wineId+'/like')
      .then(r => r.json())
      .then(data => dispatch(setCurrentLiked(data.like)))
      .catch(response => logError(response));
  }
}

export const fetchCurrentComments = (wineId) => {
  return dispatch => {
    fetch('/api/wines/'+wineId+'/comments')
      .then(r => r.json())
      .then(data => dispatch(setCurrentComments(data)))
      .catch(response => logError(response));
    }
}

export const changeCurrentLiked = (wineId, liked) => {
  return dispatch => {
    fetch(
      '/api/wines/'+wineId+'/like',
      {
        method: 'post',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({
          like: liked
        })
      })
      .catch(response => logError(response));
    if (liked) {
      dispatch(addLike());
      dispatch(setCurrentLiked(true));
    } else {
      dispatch(removeLike());
      dispatch(setCurrentLiked(false));
    }
  }
}

export const postCurrentComment = (wineId, title, body) => {
  return dispatch => {
    fetch(`/api/wines/${wineId}/comments`, {
        method: 'post',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({
          title: title,
          content: body
        })
      })
      .then(() => dispatch(fetchCurrentComments(wineId)))
      .catch(response => logError(response));
    dispatch(addComment());
  }
}

const logError = (error) => {
  console.error(error);
}
