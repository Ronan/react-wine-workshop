import React from 'react';

import { applyMiddleware, createStore } from 'redux';
import thunk from 'redux-thunk';
import app from './reducers';
import { fetchLikesCount, fetchCommentsCount } from './actions';

import { Router, Route, IndexRoute, browserHistory } from 'react-router'
import { Provider } from 'react-redux';

import PageApp      from './components/page-app';
import PageHome     from './components/page-home';
import PageRegion   from './components/page-region';
import PageWine     from './components/page-wine';
import PageNotFound from './components/page-not-found';

const store = createStore(app, applyMiddleware(thunk));
store.dispatch(fetchLikesCount());
store.dispatch(fetchCommentsCount());

const App = React.createClass({
  render() {
    return (
      /*eslint-disable react/jsx-sort-props, react/jsx-max-props-per-line */
      <Provider store={store}>
        <Router history={browserHistory}>
          <Route path="/" component={PageApp}>
            <IndexRoute component={PageHome} />
            <Route path="/regions/:regionId">
              <IndexRoute component={PageRegion} />
              <Route path="/regions/:regionId/wines/:wineId" component={PageWine} />
            </Route>
            <Route path="*" component={PageNotFound} />
          </Route>
        </Router>
      </Provider>
    );
  }
});

export default App;
