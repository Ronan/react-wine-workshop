import { combineReducers } from 'redux';
import comments from './comments';
import likes from './likes';
import title from './title';
import regions from './regions';
import wines from './wines';
import currentWine from './currentWine';

const reducer = combineReducers({
  comments: comments,
  likes: likes,
  title: title,
  regions: regions,
  wines: wines,
  currentWine : currentWine
});

export default reducer;
