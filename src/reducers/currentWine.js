const currentWine = (state = { wine: undefined, liked: false, comments: [] }, action) => {
  switch (action.type) {
    case 'SET_CURRENT_WINE':
      return Object.assign({}, state, { wine: action.wine, liked: false, comments: [] });
    case 'SET_CURRENT_LIKED':
      return Object.assign({}, state, { liked: action.liked });
    case 'SET_CURRENT_COMMENTS':
      return Object.assign({}, state, { comments: action.comments });
    default:
      return state;
  }
}

export default currentWine;
