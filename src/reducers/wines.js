const wines = (state = [], action) => {
  switch (action.type) {
    case 'SET_WINES':
      return state = action.wines ;
    default:
      return state;
  }
}

export default wines;
