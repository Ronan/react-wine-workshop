const regions = (state = [], action) => {
  switch (action.type) {
    case 'SET_REGIONS':
      return state = action.regions ;
    default:
      return state;
  }
}
export default regions;
