import React from 'react';

const PageNotFound = React.createClass({
  render() {
    return (
      <h2>Not Found</h2>
    );
  }
});

export default PageNotFound
