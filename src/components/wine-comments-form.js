import React, { PropTypes } from 'react';

import { connect } from 'react-redux';

import { postCurrentComment } from '../actions';

const mapStateToProps = (state) => {
  return {
    wineId: state.currentWine.wine.id
  };
}

const WineCommentsForm = connect(mapStateToProps)(React.createClass({

  propTypes: {
    dispatch: PropTypes.func.isRequired,
    wineId : PropTypes.string.isRequired
  },

  getInitialState() {
    return {
      commentTitle: '',
      commentBody: '',
      buttonEnabled: false
    };
  },

  updateButtonEnabled(title, body){
    if (   title.length > 0
        && body.length > 0
       ) {
      this.setState({ buttonEnabled: true });
    } else {
      this.setState({ buttonEnabled: false });
    }
  },

  handleCommentTitleChange(e) {
    this.setState({ commentTitle: e.target.value });
    this.updateButtonEnabled(e.target.value, this.state.commentBody);
  },

  handleCommentBodyChange(e) {
    this.setState({ commentBody: e.target.value });
    this.updateButtonEnabled(this.state.commentTitle, e.target.value);
  },

  handlePostComment() {
    this.props.dispatch(postCurrentComment(this.props.wineId, this.state.commentTitle, this.state.commentBody));
    this.setState({
      commentTitle: '',
      commentBody: '',
      buttonEnabled: false
    });
  },

  render() {
    return (
      <div style={{ display: 'flex', flexDirection: 'column' }}>
        <div style={{ display: 'flex', marginBottom: 10 }}>
          <input
              onChange={this.handleCommentTitleChange}
              placeholder="Comment title"
              style={{ flexGrow: 8 }}
              type="text"
              value={this.state.commentTitle}
          />
          <button
              disabled={!this.state.buttonEnabled}
              onClick={this.handlePostComment}
              style={{ flexGrow: 2, marginLeft: 10 }}
              type="button"
          >
            Comment
          </button>
        </div>
        <textarea onChange={this.handleCommentBodyChange}
            placeholder="Comment"
            rows="5"
            value={this.state.commentBody}
        ></textarea>
      </div>
    );
  }

}));

export default WineCommentsForm;
