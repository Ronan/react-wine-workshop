import React, { PropTypes } from 'react';
import { connect } from 'react-redux';

import NavWine from './nav-wine';
import WineDesc from './wine-desc';
import WineComments from './wine-comments';

import { fetchCurrentWine } from '../actions';

const mapStateToProps = (state) => {
  return {
    wine: state.currentWine.wine
  };
}

const PageWine = connect(mapStateToProps)(React.createClass({
  propTypes: {
    dispatch: PropTypes.func.isRequired,
    params: PropTypes.shape({
      regionId: PropTypes.string.isRequired,
      wineId:   PropTypes.string.isRequired
    }).isRequired,
    wine: PropTypes.shape({
      id: PropTypes.string,
      name: PropTypes.string,
      type: PropTypes.oneOf(['Rouge', 'Blanc', 'Rosé', 'Effervescent', 'Moelleux']),
      appellation: PropTypes.shape({
        name: PropTypes.string,
        region: PropTypes.string
      }),
      grapes: PropTypes.arrayOf(PropTypes.string)
    })
  },

  contextTypes: {
    router: React.PropTypes.object
  },

  componentDidMount() {
    let wineId = this.props.params.wineId;
    this.props.dispatch(fetchCurrentWine(wineId));
  },

  render () {
    let wine = this.props.wine;
    if (wine) {
      let region = wine.appellation.region;
      return (
        <div>
          <NavWine region={region}
              wine={wine.name}
          />
          <WineDesc />
          <WineComments />
        </div>
      );
    } else {
      return (<div>Loading...</div>)
    }
  }
}))

export default PageWine
