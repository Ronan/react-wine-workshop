import React from 'react';

import { Link } from 'react-router'

const NavWine = React.createClass({
  propTypes: {
    region: React.PropTypes.string,
    wine: React.PropTypes.string
  },

  render() {
    return (
      <ul>
        <li><Link to="/">Home</Link></li>
        <li><Link to={"/regions/"+this.props.region}>{this.props.region}</Link></li>
        <li>{this.props.wine}</li>
      </ul>
    );
  }
});

export default NavWine;
