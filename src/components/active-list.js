import React, { PropTypes }  from 'react';

const getClassName = function(index, selectedIndex) {
  let className = "notSelected";
  if (selectedIndex == index) {
    className = "selected";
  }
  return className;
}

const ActiveList = React.createClass({

  propTypes: {
    emptyMsg: PropTypes.string.isRequired,
    items: PropTypes.arrayOf(PropTypes.string),
    onItemSelected: PropTypes.func
  },

  getInitialState() {
    return {
      selectedIndex: -1
    };
  },

  handleItemClick(event) {
    this.setState({
      selectedIndex: event.target.getAttribute('data-tag')
    });
    if (this.props.onItemSelected != null) {
      this.props.onItemSelected(
        event.target.getAttribute('data-tag'),
        event.target.textContent
      );
    }
  },

  renderContent(){
    return (
      <ul>
      {
        this.props.items.map((item, i) =>
          <li className={getClassName(i, this.state.selectedIndex)}
              data-tag={i}
              key={i}
              onClick={this.handleItemClick}
          >
            {item}
          </li>
        )
      }
      </ul>
    )
  },

  renderEmptyContent(){
    return <div>{this.props.emptyMsg}</div>
  },

  render() {
    if (this.props.items != null && this.props.items.length > 0) {
      return this.renderContent();
    } else {
      return this.renderEmptyContent();
    }
  }
});

export default ActiveList;
