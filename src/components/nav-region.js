import React from 'react';

import { Link } from 'react-router'

const NavRegion = React.createClass({
  propTypes: {
    region: React.PropTypes.string
  },

  render() {
    return (
      <ul>
        <li><Link to="/">Home</Link></li>
        <li>{this.props.region}</li>
      </ul>
    );
  }
});

export default NavRegion;
