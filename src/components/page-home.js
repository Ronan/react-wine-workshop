import React, { PropTypes } from 'react';

import ActiveList from './active-list';
import NavHome from './nav-home';

import { connect } from 'react-redux';
import { fetchRegions, setTitle } from '../actions';

const mapStateToProps = (state) => {
  return {
    regions: state.regions
  };
}

const PageHome = connect(mapStateToProps)(React.createClass({

  propTypes: {
    dispatch: PropTypes.func.isRequired,
    regions: PropTypes.arrayOf(PropTypes.string)
  },

  contextTypes: {
    router: React.PropTypes.object
  },

  componentDidMount() {
    this.props.dispatch(setTitle('Regions'));
    this.props.dispatch(fetchRegions());
  },

  handleRegionChanged(index, region) {
    this.context.router.push({
      pathname: `/regions/${region}`
    });
  },

  render () {
    return (
      <div>
        <NavHome />
        <ActiveList
            emptyMsg="Aucune region trouvée !"
            items={this.props.regions}
            onItemSelected={this.handleRegionChanged}
        />
      </div>
    );
  }
}))

export default PageHome
