import React, { PropTypes } from 'react';
import { connect } from 'react-redux';

import { changeCurrentLiked } from '../actions';

const Styles = {
  Card: {
    padding: 8,
    boxSizing: 'border-box',
    boxShadow: '0 1px 6px rgba(0,0,0,0.12), 0 1px 4px rgba(0,0,0,0.12)',
    minHeight: 280
  },
  Title: {
    fontWeight: 'bold',
    fontSize: '1.2em'
  },
  Info: {
    marginTop: 16,
    marginBottom: 16
  },
  Label: {
    color: 'white',
    marginRight: 8,
    padding: 4,
    background: 'grey',
    borderRadius: '4px'
  },
  Image: {
    float: 'left'
  }
};

const mapStateToProps = (state) => {
  return {
    wine: state.currentWine.wine,
    liked: state.currentWine.liked
  };
}

const WineDesc = connect(mapStateToProps)(React.createClass({
  propTypes: {
    dispatch: PropTypes.func.isRequired,
    liked: PropTypes.bool.isRequired,
    wine: PropTypes.shape({
      id: PropTypes.string,
      name: PropTypes.string,
      type: PropTypes.oneOf(['Rouge', 'Blanc', 'Rosé', 'Effervescent', 'Moelleux']),
      appellation: PropTypes.shape({
        name: PropTypes.string,
        region: PropTypes.string
      }),
      grapes: PropTypes.arrayOf(PropTypes.string)
    }).isRequired
  },

  handleToggleLike() {
    this.props.dispatch(changeCurrentLiked(this.props.wine.id, !this.props.liked));
  },

  renderOpinion() {
    if (this.props.liked) {
      return ":)";
    } else {
      return ":(";
    }
  },

  render() {
    let wine = this.props.wine;
    return (
      <div className="Card">
          <img src={`/api/wines/${wine.id}/image`}
              style={Styles.Image}
          />
          <div className="Title">{wine.name}</div>
          <div className="Info">
            <span style={Styles.Label}>Type</span>{wine.type}
          </div>
          <div className="Info">
            <span style={Styles.Label}>Région</span>{wine.appellation.region}
          </div>
          <div className="Info">
            <span style={Styles.Label}>Appellation</span>{wine.appellation.name}
          </div>
          <div className="Info">
            <span style={Styles.Label}>Cépages</span>{wine.grapes.join(', ')}
          </div>
          <div className="Info">
            <span style={Styles.Label}>Mon avis</span>
            <button onClick={this.handleToggleLike}>{this.renderOpinion()}</button>
          </div>
      </div>
    )
  }
}));
export default WineDesc;
