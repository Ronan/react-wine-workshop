import React, { PropTypes } from 'react';
import { connect } from 'react-redux';

const mapStateToProps = (state) => {
  return {
    comments: state.currentWine.comments
  };
}

const WineComments = connect(mapStateToProps)(React.createClass({

  propTypes: {
    comments: React.PropTypes.arrayOf(
      PropTypes.shape({
        date: PropTypes.string,
        title: PropTypes.string,
        content: PropTypes.string
      })
    )
  },

  render() {
    if (this.props.comments) {
      return (
        <ul>
        {
          this.props.comments.map(comment =>
            <li key={comment.date.toString()}
                style={{ padding: 10, backgroundColor: '#ececec', marginTop: 5 }}
            >
              <span>{comment.title} (le <small>{comment.date}</small>)</span>
              <p>{comment.content}</p>
            </li>
          )
        }
        </ul>
      );
    } else {
      return (
        <div>
          Pas de commentaires sur ce vin...
        </div>
      )
    }
  }
}));

export default WineComments;
