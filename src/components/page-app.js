import React from 'react';
import { GlobalStats } from './global-stats';
import { connect } from 'react-redux';

const mapStateToProps = (state) => {
  return {
    title: state.title
  };
}

const PageApp = connect(mapStateToProps)(React.createClass({

  propTypes: {
    children: React.PropTypes.element.isRequired,
    title: React.PropTypes.string.isRequired
  },

  contextTypes: {
    router: React.PropTypes.object
  },

  handleGoBack() {
    this.context.router.goBack();
  },

  render () {
    return (
      <div className="grid">
        <div className="1/2 grid__cell">
          <h2>{this.props.title}</h2>
          <button onClick={this.handleGoBack}
              type="button"
          >
          back
          </button>
          {this.props.children}
        </div>
        <div className="1/2 grid__cell">
          <GlobalStats />
        </div>
      </div>
    );
  }
}))

export default PageApp
