import React from 'react';

import WineCommentsForm from './wine-comments-form';
import WineCommentsList from './wine-comments-list';

const WineComments = React.createClass({

  render() {
    return (
      <div>
        <h3>Comments</h3>
        <WineCommentsForm />
        <WineCommentsList />
      </div>
    );
  }
});

export default WineComments;
