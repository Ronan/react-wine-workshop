import React, { PropTypes } from 'react';

import NavRegion from './nav-region';
import ActiveList from './active-list';

import { connect } from 'react-redux';
import { setTitle, fetchWines } from '../actions';

const mapStateToProps = (state) => {
  let names = state.wines.map(wine => wine.name);
  let ids   = state.wines.map(wine => wine.id);
  return {
    ids  : ids,
    names: names
  };
}

const PageRegion = connect(mapStateToProps)(React.createClass({

  propTypes: {
    dispatch:   PropTypes.func.isRequired,
    ids:        PropTypes.arrayOf(PropTypes.string),
    names:      PropTypes.arrayOf(PropTypes.string),
    params: PropTypes.shape({
      regionId: PropTypes.string.isRequired
    }).isRequired
  },

  contextTypes: {
    router: React.PropTypes.object
  },

  componentDidMount() {
    this.props.dispatch(setTitle(this.props.params.regionId));
    this.props.dispatch(fetchWines(this.props.params.regionId));
  },

  handleWineChanged(index) {
    this.context.router.push({
      pathname: `/regions/${this.props.params.regionId}/wines/`+this.props.ids[index]
    });
  },

  render () {
    return (
      <div>
        <NavRegion region={this.props.params.regionId} />
        <ActiveList
            emptyMsg="Aucun vin trouvé !"
            items={this.props.names}
            onItemSelected={this.handleWineChanged}
        />
      </div>
    )
  }
}))

export default PageRegion
