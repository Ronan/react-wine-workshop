/* eslint no-undef:0 */

import React from 'react';
import { expect } from 'chai';
import { mount } from 'enzyme';

import WineCommentsList from '../../src/components/wine-comments-list';

describe('WineCommentsList', () => {
  it('Should display no comments', () => {
    const wrapper = mount(
      <WineCommentsList />
    );
    expect(wrapper.find('div').length).to.equals(1);
    expect(wrapper.find('ul').length).to.equals(0);
  });

  it('Should display 1 comment', () => {
    const comments = [{"date":"2016-05-27T14:20:49.410Z","title":"a","content":"a"}];
    const wrapper = mount(
      <WineCommentsList comments={comments}/>
    );
    expect(wrapper.find('div').length).to.equals(0);
    expect(wrapper.find('ul').length).to.equals(1);
  });

  it('Should display 3 comment', () => {
      const comments = [
        {"date":"2016-05-27T14:20:49.410Z","title":"a","content":"a"},
        {"date":"2016-05-27T15:20:49.410Z","title":"b","content":"b"},
        {"date":"2016-05-27T16:20:49.410Z","title":"c","content":"c"}
      ];
      const wrapper = mount(
        <WineCommentsList comments={comments}/>
      );
      expect(wrapper.find('div').length).to.equals(0);
      expect(wrapper.find('ul').length).to.equals(1);
      expect(wrapper.find('li').length).to.equals(3);
  });

});
