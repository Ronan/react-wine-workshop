/* eslint no-undef:0 */

import React from 'react';
import { expect } from 'chai';
import { mount } from 'enzyme';
import sinon from 'sinon';

import WineDesc from '../../src/components/wine-desc';

describe('WineDesc', () => {
  it('Should display wine information', () => {
    const wine = {
      "id": "cheval-noir",
      "name": "Cheval Noir",
      "type": "Rouge",
      "appellation": {"name": "Saint-Emilion", "region": "Bordeaux"},
      "grapes": ["Cabernet Sauvignon", "Merlot", "Cabernet Franc"]
    };
    const handleToggleLike = sinon.spy();
    const wrapper = mount(
      <WineDesc liked={false}
          onToggleLike={handleToggleLike}
          wine={wine}
      />
    );
    expect(wrapper.find('img').length).to.equal(1);
    expect(wrapper.find('div')
                  .filterWhere(div => div.props().className == "Title")
                  .first()
                  .text()).to.equal("Cheval Noir");
    expect(wrapper.find('div')
                  .filterWhere(div => div.props().className == "Info")
                  .at(0)
                  .text()).to.contain("Rouge");
    expect(wrapper.find('div')
                  .filterWhere(div => div.props().className == "Info")
                  .at(1)
                  .text()).to.contain("Bordeaux");
    expect(wrapper.find('div')
                  .filterWhere(div => div.props().className == "Info")
                  .at(2)
                  .text()).to.contain("Saint-Emilion");
    expect(wrapper.find('div')
                  .filterWhere(div => div.props().className == "Info")
                  .at(3)
                  .text()).to.contain("Cabernet Sauvignon, Merlot, Cabernet Franc");
  });

  it('Should display a like button', () => {
    const wine = {
      "id": "cheval-noir",
      "name": "Cheval Noir",
      "type": "Rouge",
      "appellation": {"name": "Saint-Emilion", "region": "Bordeaux"},
      "grapes": ["Cabernet Sauvignon", "Merlot", "Cabernet Franc"]
    };
    const handleToggleLike = sinon.spy();
    const wrapper = mount(
      <WineDesc liked={false}
          onToggleLike={handleToggleLike}
          wine={wine}
      />
    );
    expect(wrapper.find('button').length).to.equal(1);
  });

  it('Should call toggle like function', () => {
    const wine = {
      "id": "cheval-noir",
      "name": "Cheval Noir",
      "type": "Rouge",
      "appellation": {"name": "Saint-Emilion", "region": "Bordeaux"},
      "grapes": ["Cabernet Sauvignon", "Merlot", "Cabernet Franc"]
    };
    const handleToggleLike = sinon.spy();
    const wrapper = mount(
      <WineDesc liked={false}
          onToggleLike={handleToggleLike}
          wine={wine}
      />
    );
    wrapper.find('button').first().simulate('click');

    expect(handleToggleLike.calledOnce).to.equal(true);
  });

  it('Should display my unlike opinion', () => {
    const wine = {
      "id": "cheval-noir",
      "name": "Cheval Noir",
      "type": "Rouge",
      "appellation": {"name": "Saint-Emilion", "region": "Bordeaux"},
      "grapes": ["Cabernet Sauvignon", "Merlot", "Cabernet Franc"]
    };
    const handleToggleLike = sinon.spy();
    const wrapper = mount(
      <WineDesc liked={false}
          onToggleLike={handleToggleLike}
          wine={wine}
      />
    );
    expect(wrapper.find('div')
                  .filterWhere(div => div.props().className == "Info")
                  .at(4)
                  .text()).to.contain(":(");
  });

  it('Should display my like opinion', () => {
    const wine = {
      "id": "cheval-noir",
      "name": "Cheval Noir",
      "type": "Rouge",
      "appellation": {"name": "Saint-Emilion", "region": "Bordeaux"},
      "grapes": ["Cabernet Sauvignon", "Merlot", "Cabernet Franc"]
    };
    const handleToggleLike = sinon.spy();
    const wrapper = mount(
      <WineDesc liked={true}                                                    // eslint-disable-line
          onToggleLike={handleToggleLike}
          wine={wine}
      />
    );
    expect(wrapper.find('div')
                  .filterWhere(div => div.props().className == "Info")
                  .at(4)
                  .text()).to.contain(":)");
  });

});
