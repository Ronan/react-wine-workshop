/* eslint no-undef:0 */

import React from 'react';
import { expect } from 'chai';
import { mount } from 'enzyme';
import sinon from 'sinon';

import ActiveList from '../../src/components/active-list';

describe('ActiveList', () => {

  it('Should be empty', () => {
    const emptyMsg = "List is empty";
    const wrapper = mount(<ActiveList emptyMsg={emptyMsg} />);

    expect(wrapper.find('ul').length).to.equal(0);
    expect(wrapper.find('li').length).to.equal(0);

    expect(wrapper.html()).to.equal('<div>List is empty</div>');
  });


  it('Should display 1 item', () => {
    const emptyMsg = "List is empty";
    const items = [
      "Item"
    ];

    const wrapper = mount(
      <ActiveList
          emptyMsg={emptyMsg}
          items={items}
      />);

    expect(wrapper.find('ul').length).to.equal(1);
    expect(wrapper.find('li').length).to.equal(1);
    expect(wrapper.find('li').first().text()).to.equal(items[0]);
  });

  it('Should display all items', () => {
    const emptyMsg = "List is empty";
    const items = [
      "Item #1",
      "Item #2",
      "Item #3"
    ];

    const wrapper = mount(
      <ActiveList
          emptyMsg={emptyMsg}
          items={items}
      />);

    expect(wrapper.find('h2').length).to.equal(0);
    expect(wrapper.find('ul').length).to.equal(1);
    expect(wrapper.find('li').length).to.equal(3);
    expect(wrapper.find('li').at(0).text()).to.equal(items[0]);
    expect(wrapper.find('li').at(1).text()).to.equal(items[1]);
    expect(wrapper.find('li').at(2).text()).to.equal(items[2]);
  });

  it('Should use a different style for the selected item', () => {
    const emptyMsg = "List is empty";
    const items = [
      "Item #1",
      "Item #2",
      "Item #3"
    ];

    const wrapper = mount(
      <ActiveList
          emptyMsg={emptyMsg}
          items={items}
      />);

    expect(wrapper.find('li').at(0).props().className).to.equals("notSelected");
    expect(wrapper.find('li').at(1).props().className).to.equals("notSelected");
    expect(wrapper.find('li').at(2).props().className).to.equals("notSelected");

    wrapper.find('li').at(1).simulate('click');

    expect(wrapper.find('li').at(0).props().className).to.equals("notSelected");
    expect(wrapper.find('li').at(1).props().className).to.equals("selected");
    expect(wrapper.find('li').at(2).props().className).to.equals("notSelected");
  });

  it('Should call function onItemSelected', () => {
    const emptyMsg = "List is empty";
    const items = [
      "Item #1",
      "Item #2",
      "Item #3"
    ];
    const handleSelection = sinon.spy();

    const wrapper = mount(
      <ActiveList
          emptyMsg={emptyMsg}
          items={items}
          onItemSelected={handleSelection}
      />);

    wrapper.find('li').at(1).simulate('click');

    expect(handleSelection.calledOnce).to.equal(true);
    expect(handleSelection.calledWith("1", "Item #2")).to.equal(true);
  });



});
