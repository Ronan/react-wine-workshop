/* eslint no-undef:0 */

import React from 'react';
import { expect } from 'chai';
import { mount } from 'enzyme';

import WineComments from '../../src/components/wine-comments';

describe('WineComments', () => {
  it('Should display something', () => {
    const wrapper = mount(
      <WineComments />
    );
    expect(wrapper.find('div').length).to.be.at.least(1);
  });
});
