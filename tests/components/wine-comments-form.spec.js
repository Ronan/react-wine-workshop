/* eslint no-undef:0 */

import React from 'react';
import { expect } from 'chai';
import { mount } from 'enzyme';
import sinon from 'sinon';

import WineCommentsForm from '../../src/components/wine-comments-form';

describe('WineCommentsForm', () => {
  it('Should display an empty comment form', () => {
    const postComment = sinon.spy();
    const wrapper = mount(
      <WineCommentsForm onPostComment={postComment} />
    );

    expect(wrapper.find('div').length).to.be.at.least(1);
    expect(wrapper.find('input').length).to.equal(1);
    expect(wrapper.find('input').first().props().value).to.equals('');
    expect(wrapper.find('textarea').length).to.equal(1);
    expect(wrapper.find('textarea').first().props().value).to.equals('');
    expect(wrapper.find('button').length).to.equal(1);
    expect(wrapper.find('button').first().props().disabled).to.be.true;
  });

  it('Should not enable button if body remains empty', () => {
    const postComment = sinon.spy();
    const wrapper = mount(
      <WineCommentsForm onPostComment={postComment} />
    );
    wrapper.find('input').first().simulate('change', {target: {value: 'This is a title'}});
    expect(wrapper.find('input').first().props().value).to.equals('This is a title');
    expect(wrapper.find('button').first().props().disabled).to.be.true;
  });

  it('Should not enable button if title remains empty', () => {
    const postComment = sinon.spy();
    const wrapper = mount(
      <WineCommentsForm onPostComment={postComment} />
    );
    wrapper.find('textarea').first().simulate('change', {target: {value: 'This is a body'}});
    expect(wrapper.find('textarea').first().props().value).to.equals('This is a body');
    expect(wrapper.find('button').first().props().disabled).to.be.true;
  });

  it('Should enable button if title and body are not empty', () => {
    const postComment = sinon.spy();
    const wrapper = mount(
      <WineCommentsForm onPostComment={postComment} />
    );
    wrapper.find('input').first().simulate('change', {target: {value: 'This is a title'}});
    wrapper.find('textarea').first().simulate('change', {target: {value: 'This is a body'}});
    expect(wrapper.find('button').first().props().disabled).to.be.false;
  });

  it('Should be able to post a comment', () => {
    const postComment = sinon.spy();
    const wrapper = mount(
      <WineCommentsForm onPostComment={postComment} />
    );

    wrapper.find('input').first().simulate('change', {target: {value: 'This is a title'}});
    wrapper.find('textarea').first().simulate('change', {target: {value: 'This is a body'}});
    wrapper.find('button').first().simulate('click');

    expect(postComment.calledWith("This is a title", "This is a body")).to.equal(true);
    expect(wrapper.find('input').first().props().value).to.equals('');
    expect(wrapper.find('textarea').first().props().value).to.equals('');
  });


});
