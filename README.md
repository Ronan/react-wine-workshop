# react-wine-workshop

> A React.js project

``` bash
# clone project
git clone https://framagit.org/Ronan/react-wine-workshop.git

# install dependencies
npm install

# serve with hot reload at localhost:8080
npm start

# run all tests
npm test
```

API is there : https://framagit.org/Ronan/api-wine-workshop

Based upon react workshop from : https://github.com/mathieuancelin/react-workshop
